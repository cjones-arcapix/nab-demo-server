import { Column, Entity, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn } from 'typeorm';

@Entity({ name: 'datastate' })
export class State {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public filename: string;

  @Column()
  public hash: string;

  @Column()
  public ingest: boolean;

  @Column()
  public process: boolean;

  @Column()
  public staging: boolean;

  @Column()
  public production: boolean;

  @Column()
  public archive: boolean;

  @CreateDateColumn({ name: '_created' })
  public created: Date;

  @UpdateDateColumn({ name: '_updated' })
  public updated: Date;
}
