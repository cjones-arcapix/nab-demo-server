import { Logger } from '@nestjs/common';
import { OnGatewayConnection, OnGatewayInit, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';

import { Namespace, Socket } from 'socket.io';

import { StatesService } from './states.service';

@WebSocketGateway({ namespace: 'states' })
export class StatesGateway implements OnGatewayInit, OnGatewayConnection {
  /**
   * The Namesapce.
   */
  @WebSocketServer()
  private namespace: Namespace;

  /**
   * The Logger.
   */
  private logger: Logger = new Logger('StatesGateway');

  /** @ignore */
  private ms: number = 5 * 1000; // 5 seconds

  /**
   * @param statesService The StatesService.
   */
  constructor(private statesService: StatesService) {}

  /**
   * afterInit lifecycle callback.
   *
   * Invokes a recursive Function that polls the state every 5 seconds.
   */
  public afterInit(): void {
    this.logger.log('afterInit');

    this.findAll();
  }

  /**
   * handleConnection lifecycle callback.
   *
   * Handles connection Events.
   *
   * @param socket The Client.
   */
  public handleConnection(socket: Socket): void {
    this.logger.log('handleConnection');

    // Emit a 'connected' Event to the Client
    socket.emit('connected', 'Connected to Namespace');
  }

  /**
   * If there are any Clients connected to the Namespace, polls the state and emits a stateChange Event.
   *
   * Otherwise, does nothing.
   */
  private async findAll(): Promise<void> {
    const connections: number = Object.keys(this.namespace.connected).length;

    if (connections > 0) {
      this.logger.debug(`${ connections } clients connected to Namespace. Polling state.`);

      const data: any = await this.statesService.findAll();

      // Emit a 'stateChange' Event to all Clients
      this.namespace.emit('stateChange', data);
    } else {
      this.logger.debug(`No clients connected to Namespace.`);
    }

    // Re-schedule the Function.
    setTimeout(this.findAll.bind(this), this.ms);
  }
}
