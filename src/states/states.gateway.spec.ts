import { Test, TestingModule } from '@nestjs/testing';

import { StatesGateway } from './states.gateway';

describe('StatesGateway', () => {
  let gateway: StatesGateway;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StatesGateway],
    }).compile();

    gateway = module.get<StatesGateway>(StatesGateway);
  });

  describe('compilation', () => {
    it('should be defined', () => {
      expect(gateway).toBeDefined();
    });
  });
});
