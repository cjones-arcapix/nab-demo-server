import { Controller, Get } from '@nestjs/common';

import { StatesService } from './states.service';
import { State } from './state.entity';

@Controller('states')
export class StatesController {
  constructor(private statesService: StatesService) {}

  @Get()
  public async findAll(): Promise<State[]> {
    return await this.statesService.findAll();
  }
}
