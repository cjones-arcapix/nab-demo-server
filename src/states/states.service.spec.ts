import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';

import { Repository } from 'typeorm';

import { StatesService } from './states.service';
import { State } from './state.entity';

describe('StatesService', () => {
  let statesService: StatesService;
  let statesRepository: Repository<State>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        StatesService,
        {
          provide: getRepositoryToken(State),
          useValue: {
            find: () => ({})
          }
        }
      ],
    }).compile();

    statesService = module.get<StatesService>(StatesService);
    statesRepository = module.get<Repository<State>>(getRepositoryToken(State));
  });

  describe('compilation', () => {
    it('should be defined', () => {
      expect(statesService).toBeDefined();
      expect(statesRepository).toBeDefined();
    });
  });

  describe('findAll', () => {
    it('should call the Repository\'s find Function', async () => {
      const states: State[] = [
        { id: 1, filename: 'foo.txt', hash: 'foo', ingest: false, process: false, staging: false, production: false, archive: false },
        { id: 2, filename: 'bar.txt', hash: 'bar', ingest: true, process: false, staging: false, production: false, archive: false },
        { id: 3, filename: 'baz.txt', hash: 'baz', ingest: true, process: true, staging: false, production: false, archive: false },
        { id: 4, filename: 'qux.txt', hash: 'qux', ingest: true, process: true, staging: true, production: false, archive: false },
        { id: 5, filename: 'quux.txt', hash: 'quux', ingest: true, process: true, staging: true, production: true, archive: false },
        { id: 6, filename: 'quuz.txt', hash: 'quuz', ingest: true, process: true, staging: true, production: true, archive: true },
      ];

      jest.spyOn(statesRepository, 'find').mockImplementation(() => Promise.resolve(states));

      expect(await statesService.findAll()).toBe(states);
    });
  });
});
