import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { StatesController } from './states.controller';
import { StatesGateway } from './states.gateway';
import { StatesService } from './states.service';
import { State } from './state.entity';

@Module({
  controllers: [StatesController],
  imports: [TypeOrmModule.forFeature([State])],
  providers: [
    StatesGateway,
    StatesService
  ]
})
export class StatesModule {}
